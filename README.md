# superteaching

This is the web service (API) for Superensino project. 

## Pre-requisites

    $ docker
    $ docker-compose
    $ python 3+
    $ venv

## Setup

    $ python3 -m venv venv                                     # Create a virtual environment
    $ source venv/bin/activate                                 # Activate the virtual env
    $ pip install -r requirements.txt                          # Install system requirements
    $ docker-compose -f core/container/docker-compose-development.yaml up  # Starts services in docker containers (development)
    $ python manage.py migrate                                  # Execute initial migrations
    $ python manage.py createsuperuser                          # Create a superuser to admin and API usage

## Execution for development

    $ python manage.py runserver 0:8000

## Test

Just execute the pytest command, and all test cases will be discovered and executed automatically.

    $ pytest -v
