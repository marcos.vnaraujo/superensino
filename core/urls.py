"""
Core urls
"""
from django.contrib import admin
from django.urls import include, path
from .hybrid_router import HybridRouter
from exercise import views


router = HybridRouter(trailing_slash=False)
router.register(r'options', views.OptionViewSet)
router.register(r'exercises', views.ExerciseViewSet)
router.add(path(r'add-user-answer', views.UserAnswerCreateView.as_view(), name='add-user-answer'))

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns += router.urls
