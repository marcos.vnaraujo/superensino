from rest_framework import routers
from rest_framework.reverse import reverse


class HybridRouter(routers.DefaultRouter):
    """
    Router class to be used with REST framework. It provides a mechanism to register views instead of only viewsets.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.view_paths = []

    def add(self, path):
        self.view_paths.append(path)

    def get_urls(self):
        return super().get_urls() + self.view_paths

    def get_api_root_view(self, api_urls=None):
        original_view = super().get_api_root_view(api_urls)

        def view(request, *_args, **_kwargs):
            resp = original_view(request, *_args, **_kwargs)
            namespace = request.resolver_match.namespace
            for view_path in self.view_paths:
                name = view_path.name
                url_name = name
                if namespace:
                    url_name = '{}:{}'.format(namespace, url_name)
                resp.data[name] = reverse(
                    url_name, args=_args, kwargs=_kwargs, request=request, format=_kwargs.get('format', None)
                )
            return resp

        return view
