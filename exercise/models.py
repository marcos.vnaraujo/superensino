"""
Exercise models
"""
from django.contrib.auth.models import User
from django.db import models


class BaseModel(models.Model):
    """
    Base class for models
    """
    objects = models.Manager()

    class Meta:
        abstract = True


class Exercise(BaseModel):
    """
    Exercise user model
    """
    wording = models.TextField(null=True)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, related_name='exercises')

    def __str__(self):
        return f'{self.pk} - {self.wording[:50]} [{self.user}]'


class Option(BaseModel):
    """
    Option model (represents an option of an exercise)
    """
    exercise = models.ForeignKey(Exercise, on_delete=models.CASCADE, related_name='options')
    text = models.CharField(max_length=128)

    def __str__(self):
        return f'{self.pk} - {self.text[:30]}'


class ExerciseAnswer(BaseModel):
    """
    Exercise answer model (represents an exercise correct answer - inputted by admins)
    """
    option = models.OneToOneField(Option, on_delete=models.CASCADE)
    exercise = models.OneToOneField(Exercise, on_delete=models.CASCADE, related_name='exercise_answer')

    def __str__(self):
        return f'{self.exercise}:: {self.option}'


class UserAnswer(BaseModel):
    """
    User answer model (represents an exercise given answer - given by user)
    """
    option = models.OneToOneField(Option, on_delete=models.CASCADE)
    exercise = models.OneToOneField(Exercise, on_delete=models.CASCADE, related_name='user_answer')
