"""
Exercise views
"""
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

from rest_framework import exceptions, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from exercise.controllers import calculate_performance, set_user_answer
from exercise.models import Exercise, ExerciseAnswer, Option, UserAnswer
from exercise.serializers import ExerciseSerializer, OptionSerializer


class OptionViewSet(viewsets.ModelViewSet):
    """
    OptionViewSet that allows options list and retrieve
    """
    serializer_class = OptionSerializer
    permission_classes = (permissions.IsAuthenticated, )
    queryset = Option.objects.all().order_by('id')
    http_method_names = ['get']


class ExerciseViewSet(viewsets.ModelViewSet):
    """
    ExerciseViewSet that allows exercise creation, list, update and provides extra functionalities as performance report
    """

    serializer_class = ExerciseSerializer
    permission_classes = (permissions.IsAuthenticated, )
    queryset = Exercise.objects.all().order_by('id')

    @action(methods=['get'], detail=False, url_path='get-performance')
    def get_performance(self, request, pk=None):
        """
        Nested endpoint to get performance report of a user
        """
        user_id = self.request.query_params.get('user')
        user_queryset = User.objects.filter(id=user_id)

        # Validate query_params
        if not user_queryset.exists():
            raise exceptions.ParseError(_('Invalid user_id. Provided value: #{}').format(user_id))

        user = user_queryset.first()

        # Validate exercise correct answers consistency
        user_answers = UserAnswer.objects.filter(exercise__user=user)
        exercise_answers = ExerciseAnswer.objects.filter(exercise__user=user)
        if user_answers.count() > exercise_answers.count():
            raise exceptions.APIException(_('It\'s impossible to calculate the performance because not all exercises '
                                            'correct answers are registered. Please review it in admin and try again.'))

        # Calculate the report
        performance_dict = calculate_performance(user)

        return Response({
            'Total de acertos': performance_dict['correct'],
            'Total de erros': performance_dict['wrong'],
            'Aproveitamento': '{}%'.format(performance_dict['coefficient'])
        })


class UserAnswerCreateView(APIView):
    """
    API View to handle with user answer creation
    """

    permission_classes = (permissions.IsAuthenticated,)

    def put(self, request):
        # Validate query_params
        exercise_id = request.data.get('exercise')
        if not exercise_id:
            raise exceptions.ParseError(_('Missing field \'exercise\''))

        option_id = request.data.get('option')
        if not option_id:
            raise exceptions.ParseError(_('Missing field \'option\''))

        # Get instances
        exercise = Exercise.objects.get(id=exercise_id)
        option = Option.objects.get(id=option_id)

        # Set user exercise answer
        set_user_answer(exercise, option)
        return Response({'status': 'ok'}, status=status.HTTP_200_OK)
