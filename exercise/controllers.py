"""
Exercise controllers
"""
import logging
from exercise.models import UserAnswer


def set_user_answer(exercise, option):
    """
    Set user exercise answer based in provided options

    :param exercise: Exercise corresponding to the answer
    :param option: Option informed
    """
    logging.info('Setting user #{} exercise #{} answer'.format(exercise.user.id, exercise.id))

    answer_queryset = UserAnswer.objects.filter(exercise=exercise)
    if answer_queryset.exists():
        answer = answer_queryset.first()
        answer.option = option
        answer.save()
    else:
        answer = UserAnswer.objects.create(exercise=exercise, option=option)

    logging.info('The answer #{} is updated with option #{} sucessfully.'.format(answer.id, option.text[:30]))


def calculate_performance(user):
    """
    Calculate the performance report of exercises responses already provided by a user

    :param user: An user instance to calculate report of
    :return: report dict containing the: correct answers, errors, coefficient
    """
    report = {
        'correct': 0,
        'wrong': 0,
        'coefficient': 0
    }

    logging.info('Calculating user #{} performance report'.format(user.id))
    for exercise in user.exercises.all():
        user_answer = getattr(exercise, 'user_answer', None)
        exercise_answer = getattr(exercise, 'exercise_answer', None)

        if user_answer and exercise_answer and user_answer.option == exercise_answer.option:
            report['correct'] += 1
        elif user_answer and exercise_answer and user_answer.option != exercise_answer.option:
            report['wrong'] += 1

    report['coefficient'] = round((report['correct'] / user.exercises.count()) * 100)
    logging.info('User #{} performance report was calculated successfully'.format(user.id))
    return report
