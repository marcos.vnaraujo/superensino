"""
Exercise admin
"""
from django.contrib import admin
from exercise.models import Option, Exercise, ExerciseAnswer


class OptionInline(admin.TabularInline):
    model = Option


class ExerciseAnswerInline(admin.TabularInline):
    model = ExerciseAnswer


@admin.register(Exercise)
class ExerciseAdmin(admin.ModelAdmin):
    inlines = [OptionInline, ExerciseAnswerInline]
    ordering = ('id',)


admin.site.register(Option)
admin.site.register(ExerciseAnswer)
