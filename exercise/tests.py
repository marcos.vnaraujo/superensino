"""
Exercise module tests
"""
import random
import string

from django.test import TestCase
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient, APIRequestFactory, APITestCase, force_authenticate

from exercise.views import ExerciseViewSet, OptionViewSet, UserAnswerCreateView
from exercise.models import Exercise, Option, User, UserAnswer, ExerciseAnswer


def create_user():
    """
    Creates a fake user to use in unit tests
    """
    username = 'random_tester'
    password = 'super123'
    return User.objects.create_superuser(username, 'test@example.com', password)


def get_random_str(length=None):
    """
    Builds and returns a string with size N
    """
    if length is None:
        length = random.randint(10, 128)
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))


class ExerciseViewSetTest(TestCase):
    """
    Test cases to validate ExerciseViewSet
    """

    def setUp(self):
        self.user = create_user()
        self.request = APIRequestFactory().get('')
        self.viewset = ExerciseViewSet
        self.client = APIClient()

    def force_login(self):
        user = User.objects.get(username=self.user.username)
        force_authenticate(self.request, user)

    def test_not_authenticated_exercise_retrieve(self):
        exercise = Exercise.objects.create(wording='Test #1', user=self.user)
        exercise_detail = self.viewset.as_view({'get': 'retrieve'})
        response = exercise_detail(self.request, pk=exercise.pk)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_successful_exercise_retrieve(self):
        exercise = Exercise.objects.create(wording=get_random_str(), user=self.user)
        exercise_detail = self.viewset.as_view({'get': 'retrieve'})
        self.force_login()
        response = exercise_detail(self.request, pk=exercise.pk)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_successful_exercise_list(self):
        exercise_detail = ExerciseViewSet.as_view({'get': 'list'})

        exercise_list = []
        random_len = random.randint(10, 200)
        for inx in range(random_len):
            exercise_list.append(Exercise(wording=get_random_str(), user=self.user))
        Exercise.objects.bulk_create(exercise_list)

        self.force_login()
        response = exercise_detail(self.request)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], random_len)

    def test_successful_create_simple_exercise(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('exercise-list')

        data = {
            'wording': get_random_str(),
            'user': self.user.id,
        }
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_successful_update_exercise(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('exercise-list')

        data = {
            'wording': get_random_str(),
            'user': self.user.id,
        }
        response = self.client.post(url, data, format='json')
        exercise = Exercise.objects.get(id=response.data['id'])

        updated_data = {
            'wording': get_random_str(),
            'user': self.user.id
        }
        response = self.client.put(url + f'/{exercise.id}', updated_data)

        exercise.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['wording'], updated_data['wording'])


class OptionViewSetTest(TestCase):
    """
    Test cases to validate OptionViewSet (readonly)
    """

    def setUp(self):
        self.user = create_user()

        self.factory = APIRequestFactory().get('')
        self.viewset = OptionViewSet
        self.client = APIClient()

    def force_login(self, request):
        user = User.objects.get(username=self.user.username)
        force_authenticate(request, user)

    def test_successful_options_list(self):
        option_detail = OptionViewSet.as_view({'get': 'list'})
        exercise = Exercise.objects.create(wording=get_random_str(), user=self.user)

        options_list = []
        random_len = random.randint(10, 100)
        for inx in range(random_len):
            options_list.append(Option(text=get_random_str(), exercise=exercise))
        Option.objects.bulk_create(options_list)

        self.force_login(self.factory)
        response = option_detail(self.factory)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], random_len)


class UserAnswerCreateViewTestCase(APITestCase):
    """
    Test cases to validate UserAnswerCreate APIView
    """

    def setUp(self):
        self.user = create_user()
        self.url = reverse('add-user-answer')
        self.view = UserAnswerCreateView
        self.client = APIClient()

    def test_successful_user_answer(self):
        exercise = Exercise.objects.create(wording=get_random_str(), user=self.user)
        random_len = random.randint(3, 11)
        for inx in range(1, random_len):
            Option.objects.create(text='Option {}'.format(inx), exercise=exercise)

        data = {
            'exercise': exercise.id,
            'option': Option.objects.first().id
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, data)

        answer = UserAnswer.objects.first()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(answer.option.text, 'Option 1')

    def test_fail_user_anwser_with_no_option_provided(self):
        data = {'exercise': 1}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fail_user_anwser_with_no_exercise_provided(self):
        data = {'option': 1}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PerformanceReportTestCase(APITestCase):
    """
    Test cases to validate Performance Report
    """

    def setUp(self):
        self.user = create_user()
        self.url = reverse('add-user-answer')
        self.view = UserAnswerCreateView
        self.client = APIClient()

    def test_get_performance_report(self):
        for exercise_inx in range(10):
            exercise = Exercise.objects.create(wording=get_random_str(), user=self.user)

            last_option = None
            for option_inx in range(2):
                last_option = Option.objects.create(text='Option {}'.format(option_inx), exercise=exercise)

            # Set last option as exercise answer for all options
            ExerciseAnswer.objects.create(exercise=exercise, option=last_option)

            # Ensure 2 wrong options, 4 correct and all the other will be None
            if exercise_inx < 2:  # 2 wrong answers
                UserAnswer.objects.create(
                    exercise=exercise,
                    option=Option.objects.filter(exercise=exercise).first()
                )
            elif 1 < exercise_inx < 6:  # 4 correct answers
                UserAnswer.objects.create(exercise=exercise, option=last_option)

        url = reverse('exercise-list')
        url = f'{url}/get-performance'
        payload = {'user': self.user.id}
        self.client.force_authenticate(user=self.user)
        response = self.client.get(url, payload)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['Total de acertos'], 4)
        self.assertEqual(response.data['Total de erros'], 2)
        self.assertEqual(response.data['Aproveitamento'], '40%')
