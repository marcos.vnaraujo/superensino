from rest_framework import serializers
from exercise.models import Option, Exercise


class OptionSerializer(serializers.ModelSerializer):
    """
    Serializer for exercises options
    """
    class Meta:
        model = Option
        fields = ['text']


class ExerciseSerializer(serializers.ModelSerializer):
    """
    Serializer for exercises
    """

    options = OptionSerializer(many=True, read_only=True)
    exercise_answer_id = serializers.IntegerField(source='exercise_answer.option.id', read_only=True)
    exercise_answer_option = serializers.CharField(source='exercise_answer.option.text', read_only=True)
    user_answer_id = serializers.IntegerField(source='user_answer.option.id', read_only=True)
    user_answer_option = serializers.CharField(source='user_answer.option.text', read_only=True)

    class Meta:
        model = Exercise
        fields = ['id', 'wording', 'user', 'options',  'exercise_answer_id', 'exercise_answer_option',
                  'user_answer_id', 'user_answer_option']
